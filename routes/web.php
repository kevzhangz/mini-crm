<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\EmployeeLoginController;
use App\Http\Controllers\CompanyEmployeeController;
use App\Http\Controllers\EmployeeDashboardController;
use App\Http\Controllers\SellController;
use App\Http\Controllers\SellSummaryController;

Route::redirect('/', '/login');
Auth::routes(['register' => false]);
Route::get('lang/{lang}', [LocalizationController::class, 'lang'])->name('lang.switch');

Route::prefix('employee')->group(function () {
    Route::middleware('guest:employee')->group(function() {
        Route::get('login/{id}', [EmployeeLoginController::class, 'login'])->name('login.employee_view');
        Route::post('login/{id}', [EmployeeLoginController::class, 'store'])->name('login.employee');
    });
    
    Route::middleware('auth:employee')->group(function() {
        Route::post('logout', [EmployeeLoginController::class, 'logout'])->name('employee.logout');
        Route::get('dashboard/home', [EmployeeDashboardController::class, 'index'])->name('employee.dashboard');
        Route::get('dashboard/list', [EmployeeDashboardController::class, 'show']);
    });
});

Route::prefix('dashboard')->middleware('auth:web')->group(function () {
    Route::get('home',[DashboardController::class,'index']);
    Route::get('company/{company}/show-employee', [CompaniesController::class, 'showEmployee'])->name('company.employee');
    Route::get('summary', [SellSummaryController::class, 'index'])->name('summary.index');
    Route::get('summary-detail/{employee_id}/{date}', [SellSummaryController::class, 'detail'])->name('summary.detail');
    Route::resource('employee', CompanyEmployeeController::class);
    Route::resource('company', CompaniesController::class);
    Route::resource('item', ItemController::class);
    Route::resource('sell', SellController::class);
});



