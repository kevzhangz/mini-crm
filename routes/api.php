<?php

use App\Http\Controllers\AuthApiController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/login', [AuthApiController::class, 'login']);


Route::middleware('auth:sanctum')->group(function() {
    Route::get('/companies', [CompaniesApiController::class, 'index']);
    Route::get('/companies/{id}', [CompaniesApiController::class, 'search']);
    Route::post('/logout', [AuthApiController::class, 'logout']);
});
