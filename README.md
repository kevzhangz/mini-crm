## Installation

Clone this repository, install the dependencies, and setup your .env file.

```
git clone https://gitlab.com/kevzhangz/mini-crm.git
composer install
rename .env.example to .env
configure .env to your mailtrap/mailgun mailhost
```

Make a schema named mini_crm

```
php artisan migrate:fresh --seed
php artisan db:seed TranslationSeeder
php artisan storage:link
```

Run the website

```
php artisan key:generate
php artisan serve
```

Run the test

```
php artisan test
```