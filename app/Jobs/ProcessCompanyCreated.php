<?php

namespace App\Jobs;

use App\Models\Companies;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use App\Notifications\CompanyCreated;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ProcessCompanyCreated implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $companies;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Companies $companies)
    {
        $this->companies = $companies->withoutRelations();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $email = new CompanyCreated();
        // Mail::to($this->companies['email'])->send($email);
    }
}
