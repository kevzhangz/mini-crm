<?php

namespace App\Http\Controllers;


use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Http\Request;

class CompaniesApiController extends Controller
{
    public function index()
    {
        return response()->json(['messages' => 'All Company Found','companies' => Companies::all()]);
    }
    
    public function search($id)
    {
        if($company = Companies::find($id)) {
            return response()->json(['messages' => 'Company Found!', 'employee' => Employee::where('companies_id', $company['id'])->get()]);
        } else {
            return response()->json(['error' => 'Company not found!']);
        }
    }
}
