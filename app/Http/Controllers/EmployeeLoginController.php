<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeeLoginController extends Controller
{
    public function login($id)
    {
        $company = Companies::find($id);
        if(!$company) abort(404);

        return view('auth.employee-login', compact('company'));
    }

    public function store(Request $request, $id)
    {
        $credentials = $request->validate([
                        'email' => 'required',
                        'password' => 'required'
                    ]);

        $employee = Employee::where([
                    'companies_id' => $id,
                    'email' => $request->email
                    ])->first();

        if(!$employee) return back()->withErrors(['email' => __('auth.failed')]);

        if(Auth::guard('employee')->attempt($credentials)) {
            $request->session()->regenerate();
            
            return redirect()->route('employee.dashboard');
        };
        
        return back()->withErrors(['email' => __('auth.failed')]);
    }

    public function logout(Request $request)
    {
        $id = auth()->user()->companies_id;

        Auth::guard('employee')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('login.employee_view', $id);
    }
}
