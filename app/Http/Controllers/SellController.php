<?php

namespace App\Http\Controllers;

use App\Models\Item;
use App\Models\Sell;
use App\Models\Employee;
use App\Models\SellSummary;
use Illuminate\Http\Request;

class SellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->paginate;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $query = $request->string;

        if($filter = $request->filter) {
            $sells = Sell::filter($filter, $query, $start_date, $end_date)->latest()->paginate($page)->withQueryString();
        } else {
            $sells = Sell::latest()->paginate(5);
        }

        return view('dashboard.sell.index', compact('sells'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.sell.create', [
            'items' => Item::all(),
            'employees' => Employee::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date' => 'required',
            'item_id' => 'required',
            'price' => 'required',
            'discount' => 'required',
            'employee_id' => 'required'
        ]);

        Sell::create($request->all());

        $date = date('Y-m-d',strtotime($request->date));

        if( $summary = SellSummary::where(['employee_id' => $request->employee_id, 'date' => $date])->first()) {
            $summary->update([
                'price_total' => $summary->price_total + $request->price,
                'discount_total' => $summary->discount_total + ($request->price * $request->discount),
                'total' => $summary->total + $request->price - ($request->price * $request->discount),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            SellSummary::create([
                'date' => $date,
                'employee_id' => $request->employee_id,
                'price_total' => $request->price,
                'discount_total' => ($request->price * $request->discount),
                'total' => $request->price - ($request->price * $request->discount)
            ]);
        }

        return redirect()->route('sell.index')->with('success', __('flash.sell_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function show(Sell $sell)
    {
        return view('dashboard.sell.detail', compact('sell'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function edit(Sell $sell)
    {
        return view('dashboard.sell.update', [
            'items' => Item::all(),
            'employees' => Employee::all(),
            'sell' => $sell
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sell $sell)
    {
        $input = $request->validate([
            'item_id' => 'required',
            'employee_id' => 'required',
            'price' => 'required',
            'discount' => 'required'
        ]);

        if( $request->date ) $input['date'] = $request->date;
        $input['price'] = $sell->price + $request->price;
        $input['discount'] = $sell->discount + $request->discount;


        // $sell_date = date('Y-m-d',strtotime($sell->date));
        // if($sell->employee_id != $request->employee_id ) {
        //     SellSummary::updateOldSummarySellData($sell,$sell_date);
        //     SellSummary::updateNewSummarySellData($request,$sell,$sell_date);
        // };

        $sell->update($input);


        $date = date('Y-m-d',strtotime($sell->date));

        if( $summary = SellSummary::where(['employee_id' => $request->employee_id, 'date' => $date])->first()) {
            $summary->update([
                'price_total' => $summary->price_total + $request->price,
                'discount_total' => $summary->discount_total + ($sell->price * $request->discount),
                'total' => $summary->total + $request->price - ($sell->price * $request->discount),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            SellSummary::create([
                'date' => $date,
                'employee_id' => $request->employee_id,
                'price_total' => $sell->price,
                'discount_total' => $sell->discount * $sell->price,
                'total' => $sell->price - ($sell->discount * $sell->price)
            ]);
        }

        return redirect()->route('sell.index')->with('success', __('flash.sell_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sell  $sell
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sell $sell)
    {
        // $sell_date = date('Y-m-d',strtotime($sell->date));
        // SellSummary::updateOldSummarySellData($sell,$sell_date);

        $sell->delete();

        return redirect()->route('sell.index')->with('success', __('flash.sell_removed'));
    }
}
