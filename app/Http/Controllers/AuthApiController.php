<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\PersonalAccessToken;

class AuthApiController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if(!Auth::attempt($request->only('email','password'))) {
            return response()->json(['messages' => 'Invalid Credentials']);
        }

        $token = Auth::user()->createToken('ApiToken')->plainTextToken;
        
        return response()->json([
            'messages' => 'Success',
            'token' => $token
        ]);
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return response()->json(['messages' => 'Logged out!']);
    }
}
