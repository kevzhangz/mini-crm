<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Http\Request;
use App\Notifications\CompanyCreated;
use Illuminate\Support\Facades\Storage;

class CompaniesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->paginate;
        $query = $request->string;

        if($filter = $request->filter) {
            if($filter == 'name' || $filter == 'website' || $filter == 'created_at' || $filter == 'email') {
                $companies = Companies::filter($filter,$query,$page);
            } else {
                abort(404);
            }
        } else {
            $companies = Companies::latest()->paginate(5)->withQueryString();
        }

        return view('dashboard.company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
            'logo' => ' image|dimensions:min_width=100,min_height=100'
        ]);
        
        $input = $request->all();

        if ($request->file('logo')) {
            $input['logo'] = $request->file('logo')->store('logo');
        } else {
            $input['logo'] = "default.png";
        }

        $user = auth()->user();

        $input['created_by_id'] = $user->id;
        $input['updated_by_id'] = $user->id;

        Companies::create($input);

        $createdData = [
            'header' => 'New Company has been created!',
            'body' => 'New company named ' . $request->get('name') .  ' has been created!',
            'text' => 'Go to Company',
            'url' => url('/dashboard/company'),
            'thankyou' => 'Thank you'
        ];
        
        $user->notify(new CompanyCreated($createdData));
        
        return redirect()->route('company.index')->with('success', __('flash.company_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function show(Companies $company)
    {
        return view('dashboard.company.detail', compact('company'));
    }

    public function showEmployee(Request $request)
    {
        $page = $request->paginate;
        $company_id = $request->segment(3);
        $query = $request->string;

        if(!Companies::where('id', $company_id)->first()) abort(404);

        if($filter = $request->filter) {
            if($filter == 'first_name' || $filter == 'last_name' || $filter == 'email' || $filter == 'phone' || $filter == 'created_at') {
                $employees = Employee::filterByCompany($company_id,$filter,$query,$page);
            } else {
                abort(404);
            }
        } else {
            $employees = Employee::where('companies_id', $company_id)->paginate(5);
        }

        return view('dashboard.company.show-employee', compact('employees'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function edit(Companies $company)
    {

        return view('dashboard.company.update', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Companies $company)
    {
        $request->validate([
            'name' => 'required',
            'logo' => 'image|dimensions:min_width=100,min_height=100'
        ]);

        $input = $request->all();

        if ($request->file('logo')) {
            if($company['logo'] != 'default.png') Storage::delete($company['logo']);
            $input['logo'] = $request->file('logo')->store('logo');
        } else {
            unset($input['logo']);
        }

        $input['updated_by_id'] = auth()->user()->id;

        $company->update($input);

        return redirect()->route('company.index')->with('success', __('flash.company_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Companies  $companies
     * @return \Illuminate\Http\Response
     */
    public function destroy(Companies $company)
    {
        if($company['logo'] != 'default.png') Storage::delete($company['logo']);
        $company->delete();

        return redirect()->route('company.index')->with('success', __('flash.company_removed'));
    }

}
