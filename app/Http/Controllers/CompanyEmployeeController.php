<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class CompanyEmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->paginate;
        $query = $request->string;

        if($filter = $request->filter) {
            if($filter == 'first_name' || $filter == 'last_name' || $filter == 'email' || $filter == 'phone' || $filter == 'created_at') {
                $employees = Employee::filter($filter,$query,$page);
            } else {
                abort(404);
            }
        } else {
            $employees = Employee::latest()->paginate(5)->withQueryString();
        }

        return view('dashboard.company.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->all();

        return view('dashboard.company.employee.create', [
            'company_id' => key($id)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:dns',
            'password' => 'required',
            'phone' => 'required|numeric'
        ]);

        $user = auth()->user()->id;

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $input['created_by_id'] = $user;
        $input['updated_by_id'] = $user;

        Employee::create($input);

        return redirect()->route('company.index')->with('success', __('flash.employee_created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view('dashboard.company.employee.detail', compact('employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        return view('dashboard.company.employee.update', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {   
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:dns',
            'password' => 'required',
            'phone' => 'required|numeric'
        ]);

        $input = $request->only('first_name','last_name','email','phone');
        
        if($request->password != $employee->password) $input['password'] = bcrypt($request->password);

        $input['updated_by_id'] = auth()->user()->id;

        $employee->update($input);

        return redirect()->route('employee.index')->with('success', __('flash.employee_updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employee.index')->with('success', __('flash.employee_removed'));
    }
}
