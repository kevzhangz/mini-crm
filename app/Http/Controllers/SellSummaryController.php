<?php

namespace App\Http\Controllers;

use App\Models\Sell;
use App\Models\SellSummary;
use Illuminate\Http\Request;

class SellSummaryController extends Controller
{
    public function index(Request $request)
    {
        $page = $request->paginate;
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $query = $request->string;

        if( $filter = $request->filter ) {
            $summaries = SellSummary::filter($filter,$query,$start_date,$end_date,$page);
        } else {
            $summaries = SellSummary::latest()->paginate(5);
        }

        return view('dashboard.sell-summary.index', compact('summaries'));
    }

    public function detail(Request $request, $employee_id,$date)
    {
        $sells = Sell::filterSellDetail($employee_id,$date);

        return view('dashboard.sell-summary.detail', compact('sells'));
    }
}
