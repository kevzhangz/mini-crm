<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeDashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }

    public function show(Request $request)
    {
        $page = $request->paginate;
        $company_id = auth()->user()->companies_id;
        $query = $request->string;

        if($filter = $request->filter) {
            if($request->filter == 'first_name') {
                $employees = Employee::filterByCompany($company_id,$filter,$query,$page);
            } else {
                abort(404);
            }
        } else {
            $employees = Employee::where('companies_id', $company_id)->paginate(5);
        }

        return view('dashboard-employee.list', compact('employees'));
    }
}
