<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellSummary extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $with = ['employee'];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public static function filter($filter,$query,$start_date,$end_date,$page)
    {
        if( $filter == 'employee' ) {
            $employee = Employee::where('first_name', 'like', "%$query%")->first();
            if(!$start_date && !$end_date) {
                return self::where('employee_id', $employee->id)->latest()->paginate($page)->withQueryString();
            } else {
                return self::where('employee_id', $employee->id)->whereBetween('date', [$start_date,$end_date])->latest()->paginate($page)->withQueryString();
            }
        } 
        elseif ( $filter == 'company' ) {
            $company = Companies::where('name', 'like', "%$query%")->first();
            if(!$company) abort(404);

            $id = Employee::where('companies_id', $company->id)->pluck('id');

            if(!$start_date && !$end_date) {
                return self::whereIn('employee_id', $id)->latest()->paginate($page)->withQueryString();
            } else {
                return self::whereBetween('date', [$start_date,$end_date])->whereIn('employee_id', $id)
                ->latest()->paginate($page)->withQueryString();
            }
        }
        elseif ( $filter == 'date') {
            return self::whereBetween('date', [$start_date,$end_date])->latest()->paginate($page)->withQueryString();
        } else {
            abort(404);
        }
    }

    public static function updateOldSummarySellData($sell,$sell_date)
    {
        $old_summary = self::where(['employee_id' => $sell->employee_id, 'date' => $sell_date])->first();
        $old_summary->update([
            'price_total' => $old_summary->price_total - $sell->price,
            'discount_total' => $old_summary->discount_total - $sell->discount,
            'total' => ($old_summary->price_total - $sell->price) - ($old_summary->discount_total - $sell->discount),
            'updated_at' => date('Y-m-d')
        ]);
    }

    public static function updateNewSummarySellData($request,$sell,$sell_date)
    {
        if($new_summary = self::where(['employee_id' => $request->employee_id, 'date' => $sell_date])->first()) {
            $new_summary->update([
                'price_total' => $new_summary->price_total + $sell->price,
                'discount_total' => $new_summary->discount_total + $sell->discount,
                'total' => ($new_summary->price_total + $sell->price) - ($new_summary->discount_total + $sell->discount),
                'updated_at' => date('Y-m-d')
            ]);
        }
    }
}
