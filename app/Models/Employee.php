<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Employee extends Authenticatable
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $hidden = ['password'];
    protected $with = ['companies'];

    public function companies()
    {
        return $this->belongsTo(Companies::class);
    }

    public function created_by()
    {
        return $this->belongsTo(User::class);
    }

    public function updated_by()
    {
        return $this->belongsTo(User::class);
    }

    public function sell()
    {
        return $this->hasMany(Sell::class);
    }

    public function sellsummary()
    {
        return $this->hasMany(SellSummary::class);
    }

    public static function filter($filter,$query,$page)
    {
        return self::where("$filter", 'like', "%$query%")->latest()->paginate($page)->withQueryString();
    }

    public static function filterByCompany($id,$filter,$query,$page)
    {
        return self::where([['companies_id', $id], ["$filter", 'like', "%$query%"]])->latest()->paginate($page)->withQueryString();
    }
}
