<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sell extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $with = ['employee','item'];

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public static function filter($filter,$query,$start_date,$end_date)
    {
        if($filter == 'price' || $filter == 'discount') {
            if(!$start_date && !$end_date) {
                return self::where("$filter", 'like', "%$query%");
            } else {
                return self::where("$filter", 'like', "%$query%")->whereBetween('date', [$start_date,$end_date]);
            }
        }
        elseif($filter == 'date') {
            return self::whereBetween('date', [$start_date,$end_date]);
        } else {
            abort(404);
        }
    }

    public static function filterSellDetail($employee_id,$date)
    {
        return self::where([
            ['employee_id', $employee_id],
            ['date', 'like', "%$date%"]
        ])->latest()->paginate(5)->withQueryString();
    }
}
