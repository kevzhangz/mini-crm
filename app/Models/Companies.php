<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }

    public function created_by()
    {
        return $this->belongsTo(User::class);
    }

    public function updated_by()
    {
        return $this->belongsTo(User::class);
    }

    public static function filter($filter,$query,$page)
    {
        if(!$page) {
            return self::where("$filter", 'like', "%$query%")->latest()->paginate(5)->withQueryString();
        } else {
            return self::where("$filter", 'like', "%$query%")->latest()->paginate($page)->withQueryString();
        }
    }
}
