<?php

namespace Tests;

use App\Models\User;
use Illuminate\Contracts\Console\Kernel;

trait CreatesApplication
{
    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    public function signIn($user = null, $guard = null)
    {
        $user = $user ?: create(User::class);

        $this->actingAs($user, $guard);

        return $this;
    }
}
