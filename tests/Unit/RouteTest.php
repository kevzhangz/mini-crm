<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class RouteTest extends TestCase
{
    use WithFaker,RefreshDatabase;

    /** @test */
    public function redirect_default_page_to_login()
    {
        $this->get('/')->assertSee('login');
    }

    /** @test */
    public function register_page_is_not_available()
    {
        $this->get('/register')->assertStatus(404);
    }

    /** @test */
    public function change_website_language_to_indonesia()
    {
        $this->get('/lang/id')->assertSessionHas('locale','id');
    }

    /** @test */
    public function change_website_language_to_english()
    {
        $this->get('/lang/en')->assertSessionHas('locale','en');
    }

    /** @test */
    public function redirect_to_login_page_if_not_logged_in_as_admin()
    {
        $this->get('/dashboard/home')->assertRedirect('/login');
    }

    /** @test */
    public function redirect_to_login_page_if_not_logged_in_as_employee()
    {
        $this->get('/employee/dashboard/home')->assertRedirect('/login');
    }

    /** @test */
    public function admin_cannot_view_login_page_after_logged_in()
    {
        $this->signIn()->get('/login')->assertRedirect('/dashboard/home');
    }

    /** @test */
    public function employee_cannot_view_login_page_after_logged_in()
    {
        $this->signIn(create(Employee::class),'employee')->get('/employee/login/1')->assertStatus(302);
    }

    /** @test */
    public function admin_cannot_view_employee_dashboard()
    {
        $this->signIn()->get('/employee/dashboard/home')->assertStatus(302);
    }

    /** @test */
    public function employee_cannot_view_admin_dashboard()
    {
        $this->signIn(create(Employee::class),'employee')->get('/dashboard/home')->assertStatus(302);
    }

    /** @test */
    public function employee_login_page_is_available()
    {
        $company = create(Companies::class);

        $this->get("/employee/login/$company->id")->assertStatus(200);
    }
}
