<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompaniesApiControllerTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function show_all_company()
    {
        $this->signIn();

        $response = $this->getJson('api/companies');

        $response->assertJsonStructure([
            'messages',
            'companies'
        ]);
    }

    /** @test */
    public function view_company_employee()
    {
        $user = create(User::class);
        $company = create(Companies::class);

        Employee::factory(5)->create([
            'companies_id' => $company->id,
            'created_by_id' => $user->id,
            'updated_by_id' => $user->id
        ]);

        $response = $this->signIn()->getJson("api/companies/$company->id");

        $response->assertJsonStructure([
            'messages',
            'employee'
        ]);
    }

    /** @test */
    public function return_error_messages_if_company_is_not_found()
    {
        $response = $this->signIn()->getJson("api/companies/90");

        $response->assertJsonStructure(['error']);
    }
}
