<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthApiControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function email_and_password_field_required_to_login()
    {
        $response = $this->post('api/login');

        $response->assertSessionHasErrors(['email','password']);
    }

    /** @test */
    public function get_token_after_login_api()
    {
        $user = create(User::class);

        $data = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $response = $this->postJson('api/login', $data);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'messages',
            'token'
        ]);

        $this->assertAuthenticated();
    }

    /** @test */
    public function delete_token_after_logout()
    {
        $this->signIn();

        $response = $this->post('api/logout');

        $response->assertSeeText('Logged out');
    }
}
