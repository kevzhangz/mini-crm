<?php

namespace Tests\Unit;

use Exception;
use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompaniesControllerTest extends TestCase
{
    use WithFaker,RefreshDatabase;

    /** @test */
    public function admin_can_view_company_index()
    {
        $this->signIn()->get('/dashboard/company')->assertStatus(200);
    }

    /** @test */
    public function employee_cannot_view_company_index()
    {
        $this->signIn(create(Employee::class), 'employee')->get('/dashboard/company')->assertStatus(302);
    }

    /** @test */
    public function create_company_feature()
    {
        $company = make(Companies::class)->toArray();
        unset($company['logo']);

        $response = $this->signIn()->post(route('company.store'), $company);

        $response->assertSessionHas('success');
    }

    /** @test */
    public function name_and_email_and_website_field_required_to_create_company()
    {
        $company = make(Companies::class, [
            'name' => null,
            'email' => null,
            'website' => null
        ])->toArray();

        $response = $this->signIn()->post(route('company.store'), $company);

        $response->assertSessionHasErrors(['name','email','website']);
    }

    /** @test */
    public function create_employee_feature()
    {
        $employee = make(Employee::class)->toArray();
        $employee['password'] = 'password';

        $response = $this->signIn()->post(route('employee.store'), $employee);
        
        $response->assertSessionHas('success');
    }

    /** @test */
    public function first_name_and_last_name_required_to_create_employee()
    {
        $employee = make(Employee::class, [
            'first_name' => null,
            'last_name' => null
        ])->toArray();
        
        $response = $this->signIn()->post(route('employee.store'), $employee);

        $response->assertSessionHasErrors(['first_name','last_name']);
    }

    /** @test */
    public function show_company_feature()
    {
        $company = create(Companies::class);

        $response = $this->signIn()->get(route('company.show', $company));

        $response->assertSeeText($company->name);
    }

    /** @test */
    public function show_company_employee_feature()
    {
        $employee = create(Employee::class, ['first_name' => 'Kevin']);
        
        $this->signIn()->get(route('company.employee', $employee->companies_id))->assertSeeText('Kevin');
    }

    /** @test */
    public function show_company_employee_abort_404_if_company_is_not_exist()
    {
        $this->signIn()->get(route('company.employee', 99999))->assertStatus(404);
    }

    /** @test */
    public function update_company_feature()
    {
        $company = create(Companies::class);
        $newCompany = make(Companies::class)->toArray();
        unset($newCompany['logo']);

        $response = $this->signIn()->put(route('company.update', $company), $newCompany);

        $response->assertSessionHas('success');
    }

    /** @test */
    public function name_field_required_to_update_company()
    {
        $company = create(Companies::class);
        $newCompany = make(Companies::class, ['name' => null])->toArray();

        $response = $this->signIn()->put(route('company.update', $company), $newCompany);

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function delete_company_feature()
    {
        $company = create(Companies::class);

        $response = $this->signIn()->delete(route('company.destroy', $company));
        
        $response->assertSessionHas('success');
    }

    /** @test */
    public function company_filter_feature()
    {
        $company = create(Companies::class, ['name' => 'Test Company']);

        $response = $this->signIn()->call('GET', route('company.index'), ['filter' => 'name', 'string' => 'asd']);
        $response->assertDontSeeText($company->name);
    }
}
