<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Sell;
use App\Models\Employee;
use App\Models\Companies;
use App\Models\SellSummary;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SellSummaryControllerTest extends TestCase
{
    use WithFaker,RefreshDatabase;

    /** @test */
    public function sell_summary_index_feature()
    {
        $summary = create(SellSummary::class);

        $this->signIn()->get(route('summary.index'))->assertSeeText($summary->employee->first_name);
    }

    /** @test */
    public function summary_detail_feature()
    {
        $sell = make(Sell::class);

        $this->signIn()->post(route('sell.store'), $sell->toArray());

        $summary = SellSummary::where('employee_id', $sell->employee_id)->first();

        $this->get(route('summary.detail', [$summary->employee_id,$summary->date]))->assertSeeText($sell->price);

    }

    /** @test */
    public function filter_by_date_feature()
    {
        $summary = create(SellSummary::class, [
            'date' => '2021-11-11'
        ]);

        $response = $this->signIn()->call('GET', route('summary.index'), ['filter' => 'date', 'start_date' => '2021-11-11', 'end_date' => '2021-11-11']);

        $response->assertSeeText($summary->date);
    }

    /** @test */
    public function filter_by_employee_first_name_feature()
    {
        $employee = create(Employee::class);
        $summary = create(SellSummary::class, [
            'employee_id' => $employee->id
        ]);

        $response = $this->signIn()->call('GET', route('summary.index'), ['filter' => 'employee', 'string' => "$employee->name"]);

        $response->assertSeeText($summary->employee->first_name);
    }

    /** @test */
    public function filter_by_company_name_feature()
    {
        $company = create(Companies::class);

        $employee = create(Employee::class, [
            'companies_id' => $company->id
        ]);

        create(SellSummary::class, [
            'employee_id' => $employee->id
        ]);

        $response = $this->signIn()->call('GET', route('summary.index'), ['filter' => 'company', 'string' => "$company->name"]);

        $response->assertSeeText($employee->first_name);
    }
}
