<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Sell;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SellControllerTest extends TestCase
{
    use WithFaker,RefreshDatabase;

    /** @test */
    public function create_sell_feature()
    {
        $sell = make(Sell::class);
        $response = $this->signIn()->post(route('sell.store'), $sell->toArray());

        $response->assertSessionHas('success');
    }

    /** @test */
    public function price_and_discount_and_date_required_to_create_sell_feature()
    {
        $sell = make(Sell::class, [
            'date' => null,
            'price' => null,
            'discount' => null
        ]);

        $response = $this->signIn()->post(route('sell.store'), $sell->toArray());

        $response->assertSessionHasErrors(['date','price','discount']);
    }

    /** @test */
    public function show_sell_feature()
    {
        $sell = create(Sell::class);

        $this->signIn()->get(route('sell.show', $sell))->assertSeeText($sell->employee->first_name);
    }

    /** @test */
    public function update_sell_feature()
    {
        $sell = create(Sell::class);
        $newSell = make(Sell::class);

        $response = $this->signIn()->put(route('sell.update', $sell), $newSell->toArray());

        $response->assertSessionHas('success');
    }

    /** @test */
    public function price_and_discount_field_required_update_sell()
    {
        $sell = create(Sell::class);
        $newSell = make(Sell::class, [
            'price' => null,
            'discount' => null
        ]);

        $response = $this->signIn()->put(route('sell.update', $sell), $newSell->toArray());

        $response->assertSessionHasErrors(['price','discount']);
    }

    /** @test */
    public function delete_sell_feature()
    {
        $sell = create(Sell::class);

        $this->signIn()->delete(route('sell.destroy', $sell))->assertSessionHas('success');
    }
}
