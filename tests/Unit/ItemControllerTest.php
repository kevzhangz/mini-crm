<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Item;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ItemControllerTest extends TestCase
{
    use WithFaker,RefreshDatabase;

    /** @test */
    public function create_item_feature()
    {
        $item = make(Item::class);
        $response = $this->signIn()->post(route('item.store'), $item->toArray());

        $response->assertSessionHas('success');
    }

    /** @test */
    public function name_and_price_field_required_to_create_item()
    {
        $response = $this->signIn()->post(route('item.store'));

        $response->assertSessionHasErrors(['name','price']);
    }

    /** @test */
    public function show_item_feature()
    {
        $item = create(Item::class);

        $this->signIn()->get(route('item.show',$item))->assertSeeText($item->name);
    }

    /** @test */
    public function update_item_feature()
    {
        $item = create(Item::class);
        $newItem = make(Item::class);

        $response = $this->signIn()->put(route('item.update', $item), $newItem->toArray());

        $response->assertSessionHas('success');
    }

    /** @test */
    public function name_and_price_field_required_to_update_item()
    {
        $item = create(Item::class);

        $response = $this->signIn()->put(route('item.update', $item));

        $response->assertSessionHasErrors(['name','price']);
    }

    /** @test */
    public function delete_item_feature()
    {
        $item = create(Item::class);

        $this->signIn()->delete(route('item.destroy', $item))->assertSessionHas('success');
    }
}
