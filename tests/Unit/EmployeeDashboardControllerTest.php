<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Employee;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeDashboardControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function show_other_employee_feature_is_working()
    {
        $employee = create(Employee::class);
        $loginEmployee = create(Employee::class, ['companies_id' => $employee->companies_id]);

        $this->signIn($loginEmployee, 'employee')->get('/employee/dashboard/list')->assertSeeText("$employee->name");
    }
}
