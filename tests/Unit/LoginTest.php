<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function login_with_admin_account()
    {
        $user = create(User::class);

        $login = Auth::attempt([
            'email' => $user->email,
            'password' => 'password'
        ]);

        return $this->assertTrue($login);
    }

    /** @test */
    public function login_with_employee_account()
    {
        $employee = create(Employee::class);

        $login = Auth::guard('employee')->attempt([
            'email' => $employee->email,
            'password' => 'password'
        ]);

        return $this->assertTrue($login);
    }

    /** @test */
    public function logout_as_admin()
    {
        $this->signIn()->post('/logout')->assertRedirect('/');
    }

    /** @test */
    public function logout_as_employee()
    {
        $employee = create(Employee::class);

        $this->signIn($employee,'employee')->post(route('employee.logout'))->assertRedirect("/employee/login/$employee->companies_id");
    }
}
