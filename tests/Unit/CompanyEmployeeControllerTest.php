<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CompanyEmployeeControllerTest extends TestCase
{
    use WithFaker,RefreshDatabase;

    /** @test */
    public function admin_can_view_employee_index()
    {
        $this->signIn()->get('/dashboard/employee')->assertStatus(200);
    }

    /** @test */
    public function employee_cannot_view_employee_index()
    {
        $this->signIn(create(Employee::class),'employee')->get('/dashboard/employee')->assertStatus(302);
    }

    /** @test */
    public function view_employee_feature()
    {
        $employee = create(Employee::class);

        $this->signIn()->get(route('employee.show', $employee))->assertSeeText($employee->companies->name);
    }

    /** @test */
    public function update_employee_feature()
    {
        $employee = create(Employee::class);
        $newEmployee = make(Employee::class)->toArray();
        $newEmployee['password'] = '12345';

        $response = $this->signIn()->put(route('employee.update',$employee), $newEmployee);

        $response->assertSessionHas('success');
    }

    /** @test */
    public function email_and_phone_field_is_required_to_update_employee()
    {
        $employee = create(Employee::class);
        $newEmployee = make(Employee::class, [
            'phone' => null,
            'email' => null
        ]);

        $response = $this->signIn()->put(route('employee.update',$employee), $newEmployee->toArray());

        $response->assertSessionHasErrors(['phone','email']);
    }

    /** @test */
    public function delete_employee_feature()
    {
        $employee = create(Employee::class);

        $this->signIn()->delete(route('employee.destroy', $employee))->assertSessionHas('success');
    }

    /** @test */
    public function employee_filter_feature()
    {
        $employee = create(Employee::class);

        $response = $this->signIn()->call('GET', route('employee.index'), ['filter' => 'first_name', 'string' => "$employee->first_name"]);

        $response->assertSeeText("$employee->last_name");
    }
}
