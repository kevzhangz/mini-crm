@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <a href="/dashboard/company/create" class="text-white"><button type="button" class="btn btn-dark">{{ __('messages.add_company') }}</a></button>
            </div>
            <div class="col-lg-6">
                <form action="{{ route('company.index') }}" method="GET">
                    <label for="slug" class="form-label">Paginate</label>
                    <input type="text" class="form-control mb-3" name="paginate" placeholder="{{ __('messages.paginate_per_page') }}" value="{{ request('paginate') }}">
                    <div class="mb-2">
                        <label for="timezone" class="form-label">{{ __('messages.time_zone') }}</label>
                        <select class="form-select" name="timezone">
                            @if(request('timezone'))
                            <option value="{{ request('timezone') }}" selected>{{ __('messages.selected') }} ({{ ucfirst(request('timezone')) }})</option>
                            @else
                            <option value="Etc/GMT" selected>Default (UTC)</option>
                            @endif
                            <option value="Etc/GMT">UTC</option>
                            <option value="Asia/Jakarta">Jakarta</option>
                            <option value="America/Cayenne">Cayenne</option>
                            <option value="Pacific/Honolulu">Hawaii</option>
                            <option value="Asia/Seoul">Seoul</option>
                        </select>
                    </div>
                    <div class="mb-2">
                        <label for="filter" class="form-label">Filter</label>
                        <select class="form-select" name="filter">
                            @if(request('filter'))
                            <option value="{{ request('filter') }}" selected>{{ __('messages.selected') }} ({{ __('messages.' . request('filter')) }})</option>
                            @else
                            <option value="name" selected>Default ({{ __('messages.company_name') }})</option>
                            @endif
                            <option value="name">{{ __('messages.company_name') }}</option>
                            <option value="email">{{ __('messages.email') }}</option>
                            <option value="website">Website</option>
                            <option value="created_at">{{ __('messages.created_at') }}</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="string" placeholder="{{ __('messages.search') }}" value="{{ request('string') }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-dark" type="submit">{{ __('messages.search') }}</button>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="col-lg-12">
                @if($message = Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" id="my-alert">
                        {!! session('success') !!}
                        <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    </div>
                @endif
                <table id="company" class="display table">
                    <thead class="table-dark">
                        <tr>
                            <th>No</th>
                            <th>{{ __('messages.company_name') }}</th>
                            <th>{{ __('messages.email') }}</th>
                            <th>Logo</th>
                            <th>Website</th>
                            <th>{{ __('messages.created_at') }}</th>
                            <th>{{ __('messages.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($companies as $company)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->email }}</td>
                            <td>
                                @if($company->logo == 'default.png')
                                <img src="{{ asset('img/default.png') }}" style="width: 35px;">
                                @else
                                <img src="{{ asset('storage/' . $company->logo) }}" style="width:35px"></td>
                                @endif
                            <td>{{ $company->website }}</td>
                            <td>{{ \Carbon\Carbon::parse($company->created_at)->setTimezone(request('timezone')) }}</td>
                            <td>
                                <form class="d-inline" action="{{ route('company.destroy',$company) }}" method="post">
                                    <a href="{{ route('employee.create',$company) }}" class="btn btn-primary btn-sm" alt="Create Company"><i class="fas fa-user-plus"></i></a>
                                    <a href="{{ route('company.show',$company) }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('company.edit',$company) }}" class="btn btn-dark btn-sm"><i class="fas fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('{{ __('messages.confirm_to_delete_data') }}')" ><i class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $companies->links() }}
            </div>
        </div>
    </div>
</div>
@endsection