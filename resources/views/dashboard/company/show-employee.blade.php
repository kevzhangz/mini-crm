@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2 mt-3">
        <div class="mb-3">
            <a href="{{ route('company.index') }}"><button class="btn btn-dark">{{ __('messages.back_to_company') }}</button></a>
        </div>
        <div class="col-lg-6">
            <form action="{{ url()->current() }}" method="GET">
                <label for="slug" class="form-label">Paginate</label>
                <input type="text" class="form-control mb-3" name="paginate" placeholder="{{ __('messages.paginate_per_page') }}" value="{{ request('paginate') }}">
                <div class="mb-2">
                    <label for="timezone" class="form-label">{{ __('messages.time_zone') }}</label>
                    <select class="form-select" name="timezone">
                        @if(request('timezone'))
                        <option value="{{ request('timezone') }}" selected>{{ __('messages.selected') }} ({{ ucfirst(request('timezone')) }})</option>
                        @else
                        {{-- <option value="Etc/GMT" selected>Default (UTC)</option> --}}<p>Hello</p>
                        @endif
                        <option value="Etc/GMT">UTC</option>
                        <option value="Asia/Jakarta">Jakarta</option>
                        <option value="America/Cayenne">Cayenne</option>
                        <option value="Pacific/Honolulu">Hawaii</option>
                        <option value="Asia/Seoul">Seoul</option>
                    </select>
                </div>
                <div class="mb-2">
                    <label for="filter" class="form-label">Filter</label>
                    <select class="form-select" name="filter">
                        @if(request('filter'))
                        <option value="{{ request('filter') }}" selected>{{ __('messages.selected') }} ({{ __('messages.' . request('filter')) }})</option>
                        @else
                        <option value="first_name" selected>Default ({{ __('messages.first_name') }})</option>
                        @endif
                        <option value="first_name">{{ __('messages.first_name') }}</option>
                        <option value="last_name">{{ __('messages.last_name') }}</option>
                        <option value="email">{{ __('messages.email') }}</option>
                        <option value="phone">{{ __('messages.phone') }}</option>
                        <option value="created_at">{{ __('messages.created_at') }}</option>
                    </select>
                </div>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" name="string" placeholder="{{ __('messages.search') }}" value="{{ request('string') }}">
                    <div class="input-group-append">
                        <button class="btn btn-outline-dark" type="submit">{{ __('messages.search') }}</button>
                    </div>
                </div>  
            </form>
        </div>
        <div class="col-lg-12">
            <table id="employee" class="display table">
                <thead class="table-dark">
                    <tr>
                        <th>No</th>
                        <th>{{ __('messages.first_name') }}</th>
                        <th>{{ __('messages.last_name') }}</th>
                        <th>Email</th>
                        <th>{{ __('messages.phone') }}</th>
                        <th>{{ __('messages.created_at') }}</th>
                        <th>{{ __('messages.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $employee->first_name }}</td>
                        <td>{{ $employee->last_name }}</td>
                        <td>{{ $employee->email }}</td>
                        <td>{{ $employee->phone }}</td>
                        <td>{{ \Carbon\Carbon::parse($employee->created_at)->setTimezone(request('timezone')) }}
                        <td>
                            <form class="d-inline" action="{{ route('employee.destroy',$employee) }}" method="post">
                                <a href="{{ route('employee.show',$employee) }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                                <a href="{{ route('employee.edit',$employee) }}" class="btn btn-dark btn-sm"><i class="fas fa-edit"></i></a>
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" onclick="return confirm('{{ __('messages.confirm_to_delete_data') }}')" ><i class="fas fa-trash-alt"></i></a>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $employees->links() }}
        </div>
    </div>
</div>
@endsection