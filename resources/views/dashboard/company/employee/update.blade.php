@extends('dashboard.layouts.main')

@section('container')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-7">
            <form action="{{ route('employee.update',$employee) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group mb-3">
                    <label for="first_name">{{ __('messages.first_name') }}</label>
                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" autocomplete="off" value="{{ $employee->first_name }}" required>
                    @error('first_name')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="last_name">{{ __('messages.last_name') }}</label>
                    <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" autocomplete="off" value="{{ $employee->last_name }}" required>
                    @error('last_name')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="email">{{ __('messages.email') }}</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="example@example.com" autocomplete="off" value="{{ $employee->email }}">
                    @error('email')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="password">{{ __('messages.password') }} &nbsp;</label><label class="text-danger">({{ __('messages.dont_touch_password') }})</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="abcde" autocomplete="off" value="{{ $employee->password }}">
                    @error('password')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-4">
                    <label for="phone">{{ __('messages.phone_number') }}</label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="123456789" autocomplete="off" value="{{ $employee->phone }}">
                    @error('phone')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection