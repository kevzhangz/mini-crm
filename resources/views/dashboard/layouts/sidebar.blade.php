<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      Mini CRM
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="/img/hhh.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ str_contains(auth()->guard()->getName(), 'web') ? auth()->user()->name : auth()->user()->first_name }}</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="{{ __('messages.search') }}" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          @can('admin')
          <li class="nav-item">
            <a href="/dashboard/home" class="nav-link {{ (request()->is('dashboard/home*')) ? 'active' : '' }}">
              <i class="fas fa-home nav-icon"></i>
              <p>{{ __('messages.home') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/company" class="nav-link {{ (request()->is('dashboard/company*')) ? 'active' : '' }}">
              <i class="far fa-building nav-icon"></i>
              <p>{{ __('messages.company') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/employee" class="nav-link {{ (request()->is('dashboard/employee*')) ? 'active' : '' }} ">
              <i class="fas fa-user-tie nav-icon"></i>
              <p>{{ __('messages.employee') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/item" class="nav-link {{ (request()->is('dashboard/item*')) ? 'active' : '' }} ">
              <i class="fas fa-clipboard-list nav-icon"></i>
              <p>{{ __('messages.item') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/sell" class="nav-link {{ (request()->is('dashboard/sell*')) ? 'active' : '' }} ">
              <i class="fas fa-chart-line nav-icon"></i>
              <p>{{ __('messages.sell') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/dashboard/summary" class="nav-link {{ (request()->is('dashboard/summary*')) ? 'active' : '' }} ">
              <i class="fab fa-sellsy nav-icon"></i>
              <p>{{ __('messages.sell_summary') }}</p>
            </a>
          </li>
          @endcan
          @cannot('admin')
          <li class="nav-item">
            <a href="/employee/dashboard/home" class="nav-link {{ (request()->is('employee/dashboard/home*')) ? 'active' : '' }}">
              <i class="fas fa-home nav-icon"></i>
              <p>{{ __('messages.home') }}</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/employee/dashboard/list" class="nav-link {{ (request()->is('employee/dashboard/list*')) ? 'active' : '' }} ">
              <i class="fas fa-user-tie nav-icon"></i>
              <p>{{ __('messages.employee') }}</p>
            </a>
          </li>
          @endcannot
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                {{ __('messages.simple_link') }}
                <span class="right badge badge-danger">{{ __('messages.new') }}</span>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>