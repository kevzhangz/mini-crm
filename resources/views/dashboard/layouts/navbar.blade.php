  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/dashboard" class="nav-link">{{ __('messages.home') }}</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      @if(count(config('app.languages')) > 1)
      <li class="nav-item dropdown">
          <a class="nav-link dropdown" href="#" role="button" data-toggle="dropdown" id="change_language" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ strtoupper(app()->getLocale()) }}
          </a>
          <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="change_language">
              @foreach(config('app.languages') as $langLocale => $langName)
                  <li><a class="dropdown-item" href="{{ route('lang.switch', $langLocale) }}">{{ strtoupper($langLocale) }} ({{ $langName }})</a></li>
              @endforeach
          </ul>
      </li>
      @endif
      <!-- Logout Button -->
      @if(str_contains(auth()->guard()->getName(), 'web'))
      <li class="nav-item d-none d-sm-inline-block">
        <form action="/logout" method="post">
          @csrf
          <a class="nav-link"><button class="border-0" style="background-color:white;" type="submit"><i class="fas fa-sign-out-alt"></i> {{ __('messages.logout') }}</button></a>
        </form>
      </li>
      @else
      <li class="nav-item d-none d-sm-inline-block">
        <form action="{{ route('employee.logout') }}" method="post">
          @csrf
          <a class="nav-link"><button class="border-0" style="background-color:white;" type="submit"><i class="fas fa-sign-out-alt"></i> {{ __('messages.logout') }}</button></a>
        </form>
      </li>
      @endif
    </ul>
  </nav>