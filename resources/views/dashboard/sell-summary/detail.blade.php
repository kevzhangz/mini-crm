@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2">
        <div class="row">
            <div class="col-lg-12">
                <table id="company" class="display table">
                    <thead class="table-dark">
                        <tr>
                            <th>No</th>
                            <th>{{ __('messages.item_name') }}</th>
                            <th>{{ __('messages.sell_price') }}</th>
                            <th>{{ __('messages.sell_discount') }}</th>
                            <th>{{ __('messages.sell_by') }}
                            <th>{{ __('messages.sell_date') }}</th>
                            <th>{{ __('messages.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sells as $sell)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $sell->item->name }}</td>
                            <td>Rp. {{ number_format($sell->price) }}</td>
                            <td>{{ round($sell->discount * 100) }}%</td>
                            <td>{{ $sell->employee->first_name }}</td>
                            <td>{{ \Carbon\Carbon::parse($sell->date)->setTimezone(request('timezone')) }}</td>
                            <td>
                                <form class="d-inline" action="{{ route('sell.destroy',$sell) }}" method="post">
                                    <a href="{{ route('sell.show',$sell) }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('sell.edit',$sell) }}" class="btn btn-dark btn-sm"><i class="fas fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('{{ __('messages.confirm_to_delete_data') }}')" ><i class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $sells->links() }}
            </div>
        </div>
    </div>
</div>
@endsection