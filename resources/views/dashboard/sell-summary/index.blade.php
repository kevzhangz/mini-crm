@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2">
        <div class="row">
            <div class="col-lg-6">
                <form action="{{ route('summary.index') }}" method="GET">
                    <label for="slug" class="form-label">Paginate</label>
                    <input type="text" class="form-control mb-3" name="paginate" placeholder="{{ __('messages.paginate_per_page') }}" value="{{ request('paginate') }}">
                    <div class="mb-2">
                        <div>
                            <label for="date" class="form-label">{{ __('messages.start_date') }}</label>
                            <input type="date" name="start_date" value="{{ request('start_date') }}">
                        </div>
                        <div>
                            <label for="date" class="form-label">{{ __('messages.end_date') }}</label>
                            <input type="date" name="end_date" value="{{ request('end_date') }}">
                        </div>
                    </div>
                    <div class="mb-2">
                        <label for="filter" class="form-label">Filter</label>
                        <select class="form-select" name="filter">
                            @if(request('filter'))
                            <option value="{{ request('filter') }}" selected>{{ __('messages.selected') }} ({{ __('messages.' . request('filter')) }})</option>
                            @else
                            <option value="date" selected>Default ({{ __('messages.date') }})</option>
                            @endif
                            <option value="date">{{ __('messages.date') }}</option>
                            <option value="employee">{{ __('messages.employee') }}</option>
                            <option value="company">{{ __('messages.company') }}</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="string" placeholder="{{ __('messages.search') }}" value="{{ request('string') }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-dark" type="submit">{{ __('messages.search') }}</button>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="col-lg-12">
                @if($message = Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" id="my-alert">
                        {!! session('success') !!}
                        <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    </div>
                @endif
                <table id="company" class="display table">
                    <thead class="table-dark">
                        <tr>
                            <th>No</th>
                            <th>{{ __('messages.summary_price_total') }}</th>
                            <th>{{ __('messages.summary_discount_total') }}</th>
                            <th>{{ __('messages.summary_total') }}</th>
                            <th>{{ __('messages.sell_by') }}
                            <th>{{ __('messages.date') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($summaries as $summary)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>Rp. {{ number_format($summary->price_total) }}</td>
                            <td>Rp. {{ number_format($summary->discount_total) }}</td>
                            <td>Rp. {{ number_format($summary->total) }}</td>
                            <td>{{ $summary->employee->first_name }}</td>
                            <td><a href="{{ route('summary.detail', [$summary->employee->id,$summary->date]) }}">{{ $summary->date }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $summaries->links() }}
            </div>
        </div>
    </div>
</div>
@endsection