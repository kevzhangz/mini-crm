@extends('dashboard.layouts.main')

@section('container')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-uppercase font-weight-bold">{{ $item->name }}</h3>
        </div>
        <div class="card-body">
            <ul class="mt-2">
                <li>{{ __('messages.item_name') }} : {{ $item->name }}</li>
                <li>{{ __('messages.item_price') }} : {{ $item->price }}</li>
            </ul>
        </div>
    </div>
@endsection