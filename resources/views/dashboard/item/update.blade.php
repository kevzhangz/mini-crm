@extends('dashboard.layouts.main')

@section('container')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-7">
            <form action="{{ route('item.update', $item) }}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group mb-3">
                    <label for="name">{{ __('messages.item_name') }}</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="{{ __('messages.item_name') }}" autocomplete="off" value="{{ old('name', $item->name) }}">
                    @error('name')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="price">{{ __('messages.item_price') }}</label>
                    <input type="text" class="form-control" id="price" name="price" placeholder="199" autocomplete="off" value="{{ old('price', $item->price) }}">
                    @error('price')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection