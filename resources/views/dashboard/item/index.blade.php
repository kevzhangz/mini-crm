@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <a href="{{ route('item.create') }}" class="text-white"><button type="button" class="btn btn-dark">{{ __('messages.add_item') }}</a></button>
            </div>
            <div class="col-lg-6">
                <form action="{{ route('item.index') }}" method="GET">
                    <label for="slug" class="form-label">Paginate</label>
                    <input type="text" class="form-control mb-3" name="paginate" placeholder="{{ __('messages.paginate_per_page') }}" value="{{ request('paginate') }}">
                    <div class="mb-2">
                        <label for="timezone" class="form-label">{{ __('messages.time_zone') }}</label>
                        <select class="form-select" name="timezone">
                            @if(request('timezone'))
                            <option value="{{ request('timezone') }}" selected>{{ __('messages.selected') }} ({{ ucfirst(request('timezone')) }})</option>
                            @else
                            <option value="Etc/GMT" selected>Default (UTC)</option>
                            @endif
                            <option value="Etc/GMT">UTC</option>
                            <option value="Asia/Jakarta">Jakarta</option>
                            <option value="America/Cayenne">Cayenne</option>
                            <option value="Pacific/Honolulu">Hawaii</option>
                            <option value="Asia/Seoul">Seoul</option>
                        </select>
                    </div>
                    <div class="mb-2">
                        <label for="filter" class="form-label">Filter</label>
                        <select class="form-select" name="filter">
                            @if(request('filter'))
                            <option value="{{ request('filter') }}" selected>{{ __('messages.selected') }} ({{ __('messages.' . request('filter')) }})</option>
                            @else
                            <option value="name" selected>Default ({{ __('messages.item_name') }})</option>
                            @endif
                            <option value="name">{{ __('messages.item_name') }}</option>
                            <option value="price">{{ __('messages.item_price') }}</option>
                            <option value="created_at">{{ __('messages.created_at') }}</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="string" placeholder="{{ __('messages.search') }}" value="{{ request('string') }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-dark" type="submit">{{ __('messages.search') }}</button>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="col-lg-12">
                @if($message = Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" id="my-alert">
                        {!! session('success') !!}
                        <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    </div>
                @endif
                <table id="company" class="display table">
                    <thead class="table-dark">
                        <tr>
                            <th>No</th>
                            <th>{{ __('messages.item_name') }}</th>
                            <th>{{ __('messages.item_price') }}</th>
                            <th>{{ __('messages.created_at') }}</th>
                            <th>{{ __('messages.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ \Carbon\Carbon::parse($item->created_at)->setTimezone(request('timezone')) }}</td>
                            <td>
                                <form class="d-inline" action="{{ route('item.destroy',$item) }}" method="post">
                                    <a href="{{ route('item.show',$item) }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('item.edit',$item) }}" class="btn btn-dark btn-sm"><i class="fas fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('{{ __('messages.confirm_to_delete_data') }}')" ><i class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $items->links() }}
            </div>
        </div>
    </div>
</div>
@endsection