@extends('dashboard.layouts.main')

@section('container')
<div class="content">
    <div class="container ml-2">
        <div class="row">
            <div class="col-lg-12 mb-3">
                <a href="{{ route('sell.create') }}" class="text-white"><button type="button" class="btn btn-dark">{{ __('messages.add_sell') }}</a></button>
            </div>
            <div class="col-lg-6">
                <form action="{{ route('sell.index') }}" method="GET">
                    <label for="slug" class="form-label">Paginate</label>
                    <input type="text" class="form-control mb-3" name="paginate" placeholder="{{ __('messages.paginate_per_page') }}" value="{{ request('paginate') }}">
                    <div class="mb-2">
                        <label for="timezone" class="form-label">{{ __('messages.time_zone') }}</label>
                        <select class="form-select" name="timezone">
                            @if(request('timezone'))
                            <option value="{{ request('timezone') }}" selected>{{ __('messages.selected') }} ({{ ucfirst(request('timezone')) }})</option>
                            @else
                            <option value="Etc/GMT" selected>Default (UTC)</option>
                            @endif
                            <option value="Etc/GMT">UTC</option>
                            <option value="Asia/Jakarta">Jakarta</option>
                            <option value="America/Cayenne">Cayenne</option>
                            <option value="Pacific/Honolulu">Hawaii</option>
                            <option value="Asia/Seoul">Seoul</option>
                        </select>
                    </div>
                    <div class="mb-2">
                        <div>
                            <label for="date" class="form-label">{{ __('messages.start_date') }}</label>
                            <input type="datetime-local" name="start_date" value="{{ request('start_date') }}">
                        </div>
                        <div>
                            <label for="date" class="form-label">{{ __('messages.end_date') }}</label>
                            <input type="datetime-local" name="end_date" value="{{ request('end_date') }}">
                        </div>
                    </div>
                    <div class="mb-2">
                        <label for="filter" class="form-label">Filter</label>
                        <select class="form-select" name="filter">
                            @if(request('filter'))
                            <option value="{{ request('filter') }}" selected>{{ __('messages.selected') }} ({{ __('messages.' . request('filter')) }})</option>
                            @else
                            <option value="date" selected>Default ({{ __('messages.sell_date') }})</option>
                            @endif
                            <option value="date">{{ __('messages.sell_date') }}</option>
                            <option value="price">{{ __('messages.sell_price') }}</option>
                            <option value="discount">{{ __('messages.sell_discount') }}</option>
                        </select>
                    </div>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="string" placeholder="{{ __('messages.search') }}" value="{{ request('string') }}">
                        <div class="input-group-append">
                            <button class="btn btn-outline-dark" type="submit">{{ __('messages.search') }}</button>
                        </div>
                    </div>  
                </form>
            </div>
            <div class="col-lg-12">
                @if($message = Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" id="my-alert">
                        {!! session('success') !!}
                        <span type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></span>
                    </div>
                @endif
                <table id="company" class="display table">
                    <thead class="table-dark">
                        <tr>
                            <th>No</th>
                            <th>{{ __('messages.item_name') }}</th>
                            <th>{{ __('messages.sell_price') }}</th>
                            <th>{{ __('messages.sell_discount') }}</th>
                            <th>{{ __('messages.sell_by') }}
                            <th>{{ __('messages.sell_date') }}</th>
                            <th>{{ __('messages.action') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($sells as $sell)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $sell->item->name }}</td>
                            <td>Rp. {{ number_format($sell->price) }}</td>
                            <td>{{ round($sell->discount * 100) }}%</td>
                            <td>{{ $sell->employee->first_name }}</td>
                            <td>{{ \Carbon\Carbon::parse($sell->date)->setTimezone(request('timezone')) }}</td>
                            <td>
                                <form class="d-inline" action="{{ route('sell.destroy',$sell) }}" method="post">
                                    <a href="{{ route('sell.show',$sell) }}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i></a>
                                    <a href="{{ route('sell.edit',$sell) }}" class="btn btn-dark btn-sm"><i class="fas fa-edit"></i></a>
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" onclick="return confirm('{{ __('messages.confirm_to_delete_data') }}')" ><i class="fas fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $sells->links() }}
            </div>
        </div>
    </div>
</div>
@endsection