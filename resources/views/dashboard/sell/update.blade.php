@extends('dashboard.layouts.main')

@section('container')
<div class="container mt-5">
    <div class="row">
        <div class="col-lg-7">
            <form action="{{ route('sell.update', $sell) }}" method="post" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="mb-3">
                    <label for="item_id" class="form-label">{{ __('messages.item_name') }}</label>
                    <select class="form-select" name="item_id">
                        @foreach($items as $item)
                            @if($sell->item->id == $item->id)
                                <option value="{{ $item->id }}" selected>{{ $item->name }}</option>
                            @else
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="employee_id" class="form-label">{{ __('messages.sell_by') }}</label>
                    <select class="form-select" name="employee_id">
                        @foreach($employees as $employee)
                            @if($sell->employee->id == $employee->id)
                                <option value="{{ $employee->id }}" selected>{{ $employee->first_name }}</option>
                            @else
                                <option value="{{ $employee->id }}">{{ $employee->first_name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group mb-3">
                    <label for="date">{{ __('messages.sell_date') }} &nbsp;</label><label class="text-danger">({{ __('messages.dont_touch_date') }})</label>
                    <input type="datetime-local" class="form-control" id="date" name="date" placeholder="199" autocomplete="off" value="{{ old('date', date('Y-m-d')) }}">
                    @error('date')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="price">{{ __('messages.sell_price') }} ({{ $sell->price }})</label>
                    <input type="text" class="form-control" id="price" name="price" placeholder="199" autocomplete="off" value="{{ old('price',0) }}">
                    @error('price')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group mb-3">
                    <label for="discount">{{ __('messages.sell_discount') }} ({{ $sell->discount }})</label>
                    <input type="text" class="form-control" id="discount" name="discount" placeholder="199" autocomplete="off" value="{{ old('discount',0) }}">
                    @error('discount')
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection