@extends('dashboard.layouts.main')

@section('container')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title text-uppercase font-weight-bold">{{ __('messages.sell_by') }} {{ $sell->employee->first_name }}</h3>
        </div>
        <div class="card-body">
            <ul class="mt-2">
                <li>{{ __('messages.name') }} : {{ $sell->item->name }}</li>
                <li>{{ __('messages.price') }} : {{ $sell->price }}</li>
                <li>{{ __('messages.discount') }} : {{ $sell->discount }}</li>
                <li>{{ __('messages.sell_by') }} : {{ $sell->employee->first_name }}</li>
                <li>{{ __('messages.sell_date') }} : {{ $sell->date }}</li>
            </ul>
        </div>
    </div>
@endsection