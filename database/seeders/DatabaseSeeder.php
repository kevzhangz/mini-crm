<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use bfinlay\SpreadsheetSeeder\SpreadsheetSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
        ]);

        User::insert([
            'name' => 'kevin',
            'email' => 'kevin@gmail.com',
            'password' => bcrypt('password')
        ]);

        $this->call([
            SpreadsheetSeeder::class,
        ]);

        // Companies::factory(500)->create();
        // Employee::factory(1000)->create();


    }
}
