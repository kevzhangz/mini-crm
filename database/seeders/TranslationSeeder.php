<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;

class TranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Validation
        LanguageLine::create([
            'group' => 'validation',
            'key' => 'required',
            'text' => ['en' => 'The :attribute field is required.', 'id' => 'Field :attribute wajib di isi.']
        ]);

        LanguageLine::create([
            'group' => 'validation',
            'key' => 'numeric',
            'text' => ['en' => 'The :attribute must be a number.', 'id' => 'Field :attribute harus berupa angka.']
        ]);

        LanguageLine::create([
            'group' => 'validation',
            'key' => 'email',
            'text' => ['en' => 'The :attribute must be a valid email address.', 'id' => 'Field :attribute harus berupa alamat email.']
        ]);

        LanguageLine::create([
            'group' => 'validation',
            'key' => 'image',
            'text' => ['en' => 'The :attribute must be an image.', 'id' => 'Field :attribute harus berupa gambar.']
        ]);

        LanguageLine::create([
            'group' => 'validation',
            'key' => 'dimension',
            'text' => ['en' => 'The :attribute has invalid image dimensions.', 'id' => 'Field :attribute memiliki dimensi gambar yang tidak valid']
        ]);

        // Auth
        LanguageLine::create([
            'group' => 'auth',
            'key' => 'failed',
            'text' => ['en' => 'These credentials do not match our records.', 'id' => 'Email/password yang anda masukkan salah!']
        ]);

        LanguageLine::create([
            'group' => 'auth',
            'key' => 'password',
            'text' => ['en' => 'The provided password is incorrect.', 'id' => 'Kata sandi yang anda masukkan salah!']
        ]);

        LanguageLine::create([
            'group' => 'auth',
            'key' => 'throttle',
            'text' => ['en' => 'Too many login attempts. Please try again in :seconds seconds.', 'id' => 'Terlalu banyak request login!,silahkan mencoba :seconds detik lagi.']
        ]);

        // Flash
        LanguageLine::create([
            'group' => 'flash',
            'key' => 'company_created',
            'text' => ['en' => 'Company Successfully Created!', 'id' => 'Perusahaan Berhasil ditambahkan!']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'company_updated',
            'text' => ['en' => 'Company Successfully Updated!', 'id' => 'Perusahaan Berhasil diperbarui!']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'company_removed',
            'text' => ['en' => 'Company Successfully Removed!', 'id' => 'Perusahaan Berhasil dihapus!']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'employee_created',
            'text' => ['en' => 'Employee Successfully Created!', 'id' => 'Karyawan berhasil ditambahkan']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'employee_updated',
            'text' => ['en' => 'Employee Successfully Updated!', 'id' => 'Karyawan berhasil diperbarui']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'employee_removed',
            'text' => ['en' => 'Employee Successfully Removed!', 'id' => 'Karyawan berhasil dihapus']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'item_created',
            'text' => ['en' => 'Item Successfully Created!', 'id' => 'Barang berhasil ditambahkan']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'item_updated',
            'text' => ['en' => 'Item Successfully Updated!', 'id' => 'Barang berhasil diperbarui']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'item_removed',
            'text' => ['en' => 'Item Successfully Removed!', 'id' => 'Barang berhasil dihapus']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'sell_created',
            'text' => ['en' => 'Sell Successfully Created!', 'id' => 'Penjualan berhasil ditambahkan']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'sell_updated',
            'text' => ['en' => 'Sell Successfully Updated!', 'id' => 'Penjualan berhasil diperbarui']
        ]);

        LanguageLine::create([
            'group' => 'flash',
            'key' => 'sell_removed',
            'text' => ['en' => 'Sell Successfully Removed!', 'id' => 'Penjualan berhasil dihapus']
        ]);

        // Pagination
        LanguageLine::create([
            'group' => 'pagination',
            'key' => 'previous',
            'text' => ['en' => '&laquo; Previous', 'id' => '&laquo; Sebelumnya']
        ]);

        LanguageLine::create([
            'group' => 'pagination',
            'key' => 'next',
            'text' => ['en' => 'Next &raquo;', 'id' => 'Selanjutnya &raquo;']
        ]);

        // Passwords
        LanguageLine::create([
            'group' => 'passwords',
            'key' => 'reset',
            'text' => ['en' => 'Your password has been reset!', 'id' => 'Kata sandi berhasil di reset!']
        ]);

        LanguageLine::create([
            'group' => 'passwords',
            'key' => 'sent',
            'text' => ['en' => 'We have emailed your password reset link!', 'id' => 'Kami sudah mengirimkan email untuk link reset kata sandi anda!']
        ]);

        LanguageLine::create([
            'group' => 'passwords',
            'key' => 'throttled',
            'text' => ['en' => 'Please wait before retrying.', 'id' => 'Tunggu sebentar sebelum mencoba lagi']
        ]);

        LanguageLine::create([
            'group' => 'passwords',
            'key' => 'token',
            'text' => ['en' => 'This password reset token is invalid.', 'id' => 'Token reset kata sandi sudah invalid']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'home',
            'text' => ['en' => 'Home' ,'id' => 'Beranda']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'logout',
            'text' => ['en' => 'Logout' ,'id' => 'Keluar']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'add_company',
            'text' => ['en' => 'Add Company' ,'id' => 'Tambah Perusahaan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'company',
            'text' => ['en' => 'Company' ,'id' => 'Perusahaan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'employee',
            'text' => ['en' => 'Employee' ,'id' => 'Karyawan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'dashboard',
            'text' => ['en' => 'Dashboard' ,'id' => 'Beranda']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'company_name',
            'text' => ['en' => 'Company Name' ,'id' => 'Nama Perusahaan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'insert_logo_here',
            'text' => ['en' => 'Insert Logo Here' ,'id' => 'Masukkan Logo disini']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'action',
            'text' => ['en' => 'Action' ,'id' => 'Aksi']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'first_name',
            'text' => ['en' => 'First Name' ,'id' => 'Nama Depan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'last_name',
            'text' => ['en' => 'Last Name' ,'id' => 'Nama Belakang']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'phone',
            'text' => ['en' => 'Phone' ,'id' => 'Telepon']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'search',
            'text' => ['en' => 'Search' ,'id' => 'Cari']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'view_company_employee',
            'text' => ['en' => 'View Company Employee' ,'id' => 'Lihat Karyawan Perusahaan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'phone_number',
            'text' => ['en' => 'Phone Number' ,'id' => 'Nomor Telepon']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'back_to_company',
            'text' => ['en' => 'Back to Company' ,'id' => 'Kembali ke Perusahaan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'login',
            'text' => ['en' => 'Login' ,'id' => 'Masuk']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'remember_me',
            'text' => ['en' => 'Remember Me' ,'id' => 'Ingat saya']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'password',
            'text' => ['en' => 'Password' ,'id' => 'Kata Sandi']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'forgot_your_password',
            'text' => ['en' => 'Forgot Your Password?' ,'id' => 'Lupa password anda?']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'send_password_reset_link',
            'text' => ['en' => 'Send Password Reset Link' ,'id' => 'Kirim Link Reset Kata Sandi']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'reset_password',
            'text' => ['en' => 'Reset Password' ,'id' => 'Reset Kata Sandi']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'confirm_password',
            'text' => ['en' => 'Confirm Password' ,'id' => 'Konfirmasi Kata sandi']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'email_address',
            'text' => ['en' => 'E-mail Address' ,'id' => 'Alamat Email']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'simple_link',
            'text' => ['en' => 'Simple Link' ,'id' => 'Link Sederhana']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'new',
            'text' => ['en' => 'New' ,'id' => 'Baru']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'email',
            'text' => ['en' => 'Email' ,'id' => 'Alamat Email']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'name',
            'text' => ['en' => 'Name' ,'id' => 'Nama']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'created_at',
            'text' => ['en' => 'Created At' ,'id' => 'Tanggal Bikin']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'selected',
            'text' => ['en' => 'Selected' ,'id' => 'Terpilih']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'paginate_per_page',
            'text' => ['en' => 'Paginate per Page' ,'id' => 'Paginate per Halaman']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'time_zone',
            'text' => ['en' => 'Time Zone' ,'id' => 'Zona Waktu']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'dont_touch_password',
            'text' => ['en' => 'Dont touch it if you are not changing the password' ,'id' => 'Jangan disentuh jika anda tidak ingin mengganti password']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'confirm_to_delete_data',
            'text' => ['en' => 'Confirm to delete data?' ,'id' => 'Yakin untuk menghapus data?']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'item',
            'text' => ['en' => 'Item', 'id' => 'Barang']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'item_name',
            'text' => ['en' => 'Item Name', 'id' => 'Nama Barang']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'item_price',
            'text' => ['en' => 'Item Price', 'id' => 'Harga Barang']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'add_item',
            'text' => ['en' => 'Add Item', 'id' => 'Tambah Barang']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'sell',
            'text' => ['en' => 'Sell', 'id' => 'Penjualan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'sell_price',
            'text' => ['en' => 'Sell Price', 'id' => 'Harga Penjualan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'sell_discount',
            'text' => ['en' => 'Sell Discount', 'id' => 'Diskon Penjualan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'sell_by',
            'text' => ['en' => 'Sell By', 'id' => 'Dijual oleh']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'sell_date',
            'text' => ['en' => 'Sell Date', 'id' => 'Tanggal Penjualan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'add_sell',
            'text' => ['en' => 'Add Sell', 'id' => 'Tambah Penjualan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'date',
            'text' => ['en' => 'Date', 'id' => 'Tanggal']
        ]);
        
        LanguageLine::create([
            'group' => 'messages',
            'key' => 'price',
            'text' => ['en' => 'Price', 'id' => 'Harga']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'discount',
            'text' => ['en' => 'Discount', 'id' => 'Diskon']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'dont_touch_date',
            'text' => ['en' => 'Dont Touch it if you are not changing the date', 'id' => 'Jangan disentuh jika anda tidak ingin mengganti tanggal']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'sell_summary',
            'text' => ['en' => 'Sell Summary', 'id' => 'Rangkuman Penjualan']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'summary_price_total',
            'text' => ['en' => 'Total Price', 'id' => 'Total Harga']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'summary_discount_total',
            'text' => ['en' => 'Total Discount', 'id' => 'Total Diskon']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'summary_total',
            'text' => ['en' => 'Total', 'id' => 'Total']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'start_date',
            'text' => ['en' => 'Start Date', 'id' => 'Mulai Tanggal']
        ]);

        LanguageLine::create([
            'group' => 'messages',
            'key' => 'end_date',
            'text' => ['en' => 'End Date', 'id' => 'Sampai Tanggal']
        ]);
        
    }
}
