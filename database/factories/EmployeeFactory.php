<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Employee;
use App\Models\Companies;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmployeeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Employee::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'companies_id' => Companies::factory(),
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->freeEmail(),
            'phone' => $this->faker->randomNumber(),
            'password' => bcrypt('password'),
            'created_by_id' => User::factory(),
            'updated_by_id' => User::factory()
        ];
    }
}
