<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\Sell;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;

class SellFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Sell::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'item_id' => Item::factory(),
            'employee_id' => Employee::factory(),
            'date' => date('Y-m-d H:i:s',$this->faker->unixTime()),
            'price' => $this->faker->randomNumber(3,true),
            'discount' => $this->faker->randomNumber(2,true)
        ];
    }
}
