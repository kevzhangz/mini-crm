<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\SellSummary;
use Illuminate\Database\Eloquent\Factories\Factory;

class SellSummaryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SellSummary::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => date('Y-m-d', $this->faker->unixTime()),
            'employee_id' => Employee::factory(),
            'price_total' => $this->faker->randomNumber(4,true),
            'discount_total' => $this->faker->randomNumber(3,true),
            'total' => $this->faker->randomNumber(4,true)
        ];
    }
}
