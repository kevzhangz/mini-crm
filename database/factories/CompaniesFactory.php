<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Companies;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompaniesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Companies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'email' => $this->faker->companyEmail(),
            'logo' => 'default.png',
            'website' => $this->faker->domainName(),
            'created_by_id' => User::factory(),
            'updated_by_id' => User::factory()
        ];
    }
}
